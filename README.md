# Linux Dash 中文版

一个轻量级，低负载的linux服务器监视系统。(~1MB)

## 特性
* 界面友好，低负载
* 轻量级（不到1M大小）
* 在线图表，可刷新插件，迅速增长的模块

## 安装

### 1. 下载 Linux Dash 中文版

- 直接 git clone http://git.oschina.net/yansongda/linux-dash_zh.git

- 下载 http://git.oschina.net/yansongda/linux-dash_zh/repository/archive?ref=master

### 2. 服务器环境说明

1、 首先请确认服务器中有 lap 或者 lnp 环境（linux、apache、php5+ 或者 linux、nginx、php5+）；
2、 请确保 `exec`, `shell_exec`, 和 `escapeshellarg` 函数没有被禁用

### 3. 安全

*从安全角度考虑，强烈建议为linux-dash中文版设定密码！可以使用.htaccess或者直接php验证。（设置方法不在此范围内）*


### 4. 运行 linux-dash 中文版

访问 linux dash 中文版所在网站即可。

## 说明

此项目根据国外某大神所提供项目汉化而来，同时根据天朝国情做了相应修改调整！如果有优化不好的地方，望指正！